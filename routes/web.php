<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')
    ->middleware('auth')
    ->namespace('Admin')
    ->as('admin.')
    ->group(function ($router) {
        $router->get('/', function () { return view('admin.home'); });

        $router->get('terms', 'TermsController@edit');
        $router->put('terms', 'TermsController@update')->name('terms.store');

        $router->resource('categories', CategoryController::class);
        $router->resource('structures', StructureController::class);
        $router->resource('products', ProductController::class);
        $router->resource('products.pictures', ProductPictureController::class);
        $router->resource('orders', OrderController::class);
        $router->resource('orders.details', OrderDetailController::class);

        $router->get('users/{user}/password', 'UserController@showPassword');
        $router->put('users/{user}/password', 'UserController@changePassword')->name('users.password');
        $router->resource('users', UserController::class);

        $router->get('orders/{order}/pdf-export', 'OrderController@pdfExport');

        $router->resource('settings', SettingsController::class)->only('edit', 'update');
    });

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/about-us', 'HomeController@about');
Route::get('/catalogs', 'HomeController@showCatalogs');
Route::get('/contact', 'HomeController@showContact');
Route::get('/terms', 'HomeController@showTerms');
Route::post('/contact', 'HomeController@sendContact')->name('contact.post');
Route::get('/order', 'HomeController@showOrder')->name('orders.create');
Route::post('/order', 'HomeController@saveOrder')->name('orders.store');
Route::get('/{slug}', 'CategoryController@show');
Route::get('/{categorySlug}/{productSlug}', 'ProductController@show');
