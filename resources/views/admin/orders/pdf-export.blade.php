<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
	<link rel="stylesheet" href="{{ asset('/vendors/bootstrap/css/bootstrap.min.css') }}">
	<style>
		.invoice-head td {
			padding: 0 8px;
		}
		.container {
			padding-top:30px;
		}
		.invoice-body{
			background-color:transparent;
		}
		.invoice-thank{
			margin-top: 60px;
			padding: 5px;
		}
		address{
			margin-top:15px;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<img src="{{ asset('/images/aline.png') }}" class="img-rounded logo">
				<address>
					<strong>FABRICA DE CIOCOLATA SRL</strong><br>
					CUI 39045733<br>
					Tel. 0747 372 376<br>
				</address>
			</div>
			<div class="col-md-3 well">
				<table class="invoice-head">
					<tbody>
						<tr>
							<td class="pull-right"><strong>Nr.</strong></td>
							<td>12202032</td>
						</tr>
						<tr>
							<td class="pull-right"><strong>Invoice #</strong></td>
							<td>2340</td>
						</tr>
						<tr>
							<td class="pull-right"><strong>Data</strong></td>
							<td>{{ now() }}</td>
						</tr>
						<tr>
							<td class="pull-right"><strong>Period</strong></td>
							<td>9/1/2103 - 9/30/2013</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="span8">
				<h2>Bon Comanda</h2>
			</div>
		</div>
		<div class="row">
			<div class="span8 well invoice-body">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Description</th>
							<th>Date</th>
							<th>Amount</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Service request</td>
							<td>10/8/2013</td>
							<td>$1000.00</td>
						</tr><tr>
							<td>&nbsp;</td>
							<td><strong>Total</strong></td>
							<td><strong>$1000.00</strong></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="span8 well invoice-thank">
				<h5 style="text-align:center;">Thank You!</h5>
			</div>
		</div>
		<div class="row">
			<div class="span3">
				<strong>Phone:</strong> <a href="tel:555-555-5555">555-555-5555</a>
			</div>
			<div class="span3">
				<strong>Email:</strong> <a href="mailto:hello@5marks.co">hello@bootply.com</a>
			</div>
			<div class="span3">
				<strong>Website:</strong> <a href="http://5marks.co">http://bootply.com</a>
			</div>
		</div>
	</div>

	{{-- <script src="/vendors/jquery-1.10.2.min.js"></script>
	<script src="/vendors/bootstrap/js/bootstrap.min.js"></script> --}}
</body>
</html>
