@extends('adminlte::page')

@section('content')
    {!! Form::open()->post()->route('admin.orders.store') !!}
        {!! Form::text('name', 'Nume') !!}
        {!! Form::text('email', 'Email') !!}
        {!! Form::select('with_pickup', 'Cu ridicare', [0 => 'Nu', 1 => 'Da']) !!}
        {!! Form::text('address', 'Adresa') !!}
        {!! Form::text('delivery_date', 'Data de livrare')->attrs(['class' => 'datepicker']) !!}
        {!! Form::text('phone', 'Telefon') !!}
        {!! Form::text('advance', 'Avans') !!}
        {!! Form::textarea('description', 'Descriere client') !!}
        {!! Form::submit('Salveaza') !!}
        {!! Form::anchor('Inapoi', '/admin/orders') !!}
    {!! Form::close() !!}
@endsection

@include('components.datepicker')