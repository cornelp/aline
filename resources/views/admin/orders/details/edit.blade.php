@extends('adminlte::page')

@section('content')
    {!! Form::open()->put()->route('admin.orders.details.update', [$order->id, $detail->id])->fill($detail) !!}
        {!! Form::select('product_id', 'Produs', $products) !!}
        {!! Form::select('structure_id', 'Compozitie', $structures) !!}
        {!! Form::text('quantity', 'Cantitate') !!}
        {!! Form::text('price', 'Pret') !!}
        {!! Form::text('weight', 'Greutate') !!}
        {!! Form::text('for_no_people', 'Nr Persoane') !!}
        {!! Form::textarea('message', 'Mesaj (pe tort - optional)') !!}
        {!! Form::text('decor', 'Decor') !!}
        {!! Form::text('text', 'Observatii') !!}
        {!! Form::submit('Modifica') !!}
        {!! Form::anchor('Inapoi', '/admin/orders/' . $order->id) !!}
    {!! Form::close() !!}
@endsection