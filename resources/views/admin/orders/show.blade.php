@extends('adminlte::page')

@section('content')
@box
<p>Comanda <b>{{ $order->is_processed ? '' : 'Nu' }}</b> este procesata</p>
<p>Comanda client (daca exista):</p>
<p>"{{ $order->description }}"</p>

@slot('buttons')
    {!! Form::anchor('Inapoi', '/admin/orders') !!}
    {!! Form::anchor('Adauga produse', '#')->attrs(['data-toggle' => 'modal', 'data-target' => '#modal']) !!}
    {!! Form::anchor('Modifica date comanda', '/admin/orders/' . $order->id . '/edit') !!}
    {!! Form::anchor('Export PDF', '/admin/orders/' . $order->id . '/pdf-export') !!}
    @if (! $order->is_processed)
        {!! Form::anchor('Marcheaza ca procesata', '#')->attrs(['class' => 'success', 'onclick' => 'markAsProcessed(event)']) !!}
    @endif
    @if (! $order->is_canceled)
        {!! Form::anchor('Anuleaza comanda', '#')->attrs(['class' => 'danger', 'onclick' => 'markAsCanceled(event)']) !!}
    @endif
@endslot

<hr>

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>Id</th>
            <th>Produs</th>
            <th>Cantitate</th>
            <th>Compozitie</th>
            <th>Pret</th>
            <th>Greutate</th>
            <th>Nr persoane</th>
            <th>Mesaj pe tort</th>
            <th>Observatii</th>
            <th>Creat la</th>
            <th>Modificat la</th>
            <th>Actiuni</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($order->details as $detail)
        <tr>
            <td>{{ $detail->id }}</td>
            <td>{{ $detail->product->name }}</td>
            <td>{{ $detail->quantity }}</td>
            <td>{{ $detail->structure->name }}</td>
            <td>@float($detail->price)</td>
            <td>@float($detail->weight)</td>
            <td>{{ $detail->for_no_people }}</td>
            <td>{{ $detail->message }}</td>
            <td>{{ $detail->text }}</td>
            <td>{{ $detail->created_at->format('d.m.Y H:i:s') }}</td>
            <td>{{ $detail->updated_at->format('d.m.Y H:i:s') }}</td>
            <td>
                <a href="/admin/orders/{{ $order->id }}/details/{{ $detail->id }}/edit">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="#" onclick="deleteDetail({{ $detail->id }})">
                    <i class="fa fa-close"></i>
                </a>
            </td>
            @endforeach
        </tbody>
    </table>

    <hr>

    <p>Comentarii adaugate:</p>
    <p>"{{ $order->observations }}"</p>
    @endbox

    @modal(['title' => 'Adauga produs'])
    {!! Form::open()->post()->route('admin.orders.details.store', [$order->id]) !!}
    {!! Form::hidden('order_id', $order->id) !!}
    {!! Form::select('product_id', 'Produs', $products) !!}
    {!! Form::select('structure_id', 'Compozitie', $structures) !!}
    {!! Form::text('quantity', 'Cantitate') !!}
    {!! Form::text('price', 'Pret aprox') !!}
    {!! Form::text('weight', 'Greutate') !!}
    {!! Form::text('for_no_people', 'Nr persoane') !!}
    {!! Form::textarea('message', 'Mesaj (pe tort - optional)') !!}
    {!! Form::text('weight', 'Greutate') !!}
    {!! Form::text('decor', 'Decor') !!}
    {!! Form::textarea('text', 'Observatii') !!}
    {!! Form::submit('Salveaza') !!}
    {!! Form::close() !!}
    @endmodal
    @endsection

    @push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
    <script>
        let config = {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        };

        let order = JSON.parse('{!! $order->toJson() !!}');

        let deleteDetail = function (orderId) {
            if (! confirm('Esti sigur?')) {
                return false;
            }

            axios.delete('/admin/orders/{{ $order->id }}/details/' + orderId)
                .then(function () {
                    location.reload();
                });
        };

        let markAsProcessed = function (event) {
            if (! confirm('Esti sigur?')) {
                return false;
            }

            let data = Object.assign({}, order, { is_processed: true });
            data.is_processed = true;

            axios.put('/admin/orders/{{ $order->id }}', data, config)
                .then(function (response) {
                    location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                })

            event.preventDefault();
        }

        let markAsCanceled = function (event) {
            let data = Object.assign({}, order, { is_canceled: true });

            axios.put('/admin/orders/{{ $order->id }}', data, config)
                .then(function () {
                    location.reload();
                });

            event.preventDefault();
        };
    </script>
    @endpush
