@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            {!! Form::open()->get()->route('admin.orders.index') !!}
                <div class="col-xs-6">
                    {!! Form::text('startDate', 'De la', $startDate->format('d.m.Y'))->attrs(['class' => 'datepicker', 'autocomplete' => 'off', 'onchange' => 'this.form.submit()']) !!}
                </div>
                <div class="col-xs-6">
                    {!! Form::text('endDate', 'Pana la', $endDate->format('d.m.Y'))->attrs(['class' => 'datepicker', 'autocomplete' => 'off', 'onchange' => 'this.form.submit()']) !!}
                </div>
                {!! Form::anchor('Adauga o comanda noua', '/admin/orders/create') !!}
            {!! Form::close() !!}
        @endslot

        @if ($orders->count())
            <div class="alert alert-warning col-xs-6">
                Sunt afisate comenzile cu data de livrare in intervalul
                {{ $startDate->format('d.m.Y') }} - {{ $endDate->format('d.m.Y') }}
            </div>

            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nume</th>
                        <th>Email</th>
                        <th>Telefon</th>
                        <th>Adresa</th>
                        <th>Data de livrare</th>
                        <th>E procesata</th>
                        <th>Total</th>
                        <th>Avans</th>
                        <th>Status</th>
                        <th>Creat de</th>
                        <th>Creat la</th>
                        <th>Modificat la</th>
                        <th>Actiuni</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($orders as $order)
                        <tr class="{{ $order->is_canceled ? 'danger' : ($order->is_processed ? 'success' : '') }}">
                            <td>{{ $order->id }}</td>
                            <td>
                                <a href="{{ route('admin.orders.show', $order->id) }}" class="">
                                    {{ $order->name }}
                                </a>
                            </td>
                            <td>{{ $order->email }}</td>
                            <td>{{ $order->phone }}</td>
                            <td>{{ $order->address }}</td>
                            <td>{{ $order->delivery_date }}</td>
                            {{-- <td>{{ $order->observations ? 'Da' : 'Nu' }}</td> --}}
                            <td>{!! $order->is_processed ? 'Da <i class="fa fa-check"></i>' : 'Nu' !!}</td>
                            <td>@float($order->getTotalPrice())</td>
                            <td>@float($order->advance)</td>
                            <td>{{ $order->is_canceled ? 'Anulata' : 'Valida' }}</td>
                            <td>{{ $order->owner->name }}</td>
                            <td>{{ $order->created_at->format('d.m.Y H:i:s') }}</td>
                            <td>{{ $order->updated_at->format('d.m.Y H:i:s') }}</td>
                            <td>
                                <a href="/admin/orders/{{ $order->id }}/edit" class="btn btn-xs">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $orders->links() }}
        @else
            Nu exista inregistrari
        @endif
    @endbox
@endsection

@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
@endpush

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script>
        $(function () {
            $('.datepicker').datepicker({
                format: 'dd.mm.yyyy',
                autoclose: true
            })
        });
    </script>
@endpush
