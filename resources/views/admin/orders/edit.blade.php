@extends('adminlte::page')

@section('content')
    {!! Form::open()->put()->route('admin.orders.update', [$order->id])->fill($order) !!}
        {!! Form::text('name', 'Nume') !!}
        {!! Form::text('email', 'Email') !!}
        {!! Form::text('address', 'Adresa') !!}
        {!! Form::text('phone', 'Telefon') !!}
        {!! Form::select('is_processed', 'E procesata', [0 => 'Nu', 1 => 'Da']) !!}
        {!! Form::select('is_canceled', 'E anulata', [0 => 'Nu', 1 => 'Da']) !!}
        {!! Form::select('with_pickup', 'Cu ridicare', [0 => 'Nu', 1 => 'Da']) !!}
        {!! Form::text('advance', 'Avans') !!}
        {!! Form::text('phone', 'Telefon') !!}
        {!! Form::text('delivery_date', 'Data de livrare')->attrs(['class' => 'datepicker']) !!}
        {!! Form::textarea('description', 'Descriere client') !!}
        {!! Form::textarea('observations', 'Comentarii (nu sunt adaugate de client)') !!}
        {!! Form::submit('Modifica') !!}
        {!! Form::anchor('Inapoi', '/admin/orders/') !!}
    {!! Form::close() !!}
@endsection

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
@endsection

@include('components.datepicker')