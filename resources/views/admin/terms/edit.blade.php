@extends('adminlte::page')

@section('content')
    @box
        {!! Form::open()->put()->route('admin.terms.store') !!}
            {!! Form::textarea('content', 'Continut', $content) !!}
            {!! Form::submit('Salveaza') !!}
        {!! Form::close() !!}
    @endbox
@endsection

@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/summernote-bs4.css">
@endpush

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/summernote-bs4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/lang/summernote-ro-RO.min.js"></script>
    <script>
        $(function () {
            $('#content').summernote();
        });
    </script>
@endpush