@extends('adminlte::page')

@section('content')
    {!! Form::open()->post()->route('admin.users.store') !!}
        {!! Form::text('name', 'Nume') !!}
        {!! Form::text('email', 'Email') !!}
        {!! Form::text('password', 'Parola')->type('password') !!}
        {!! Form::text('password_confirmation', 'Confirmare parola')->type('password') !!}
        {!! Form::submit('Salveaza') !!}
        {!! Form::anchor('Inapoi', '/admin/users') !!}
    {!! Form::close() !!}
@endsection