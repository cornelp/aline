@extends('adminlte::page')

@section('content')
    {!! Form::open()->put()->route('admin.users.password', [$user->id]) !!}
        {!! Form::text('password', 'Parola')->type('password') !!}
        {!! Form::text('password_confirmation', 'Confirmare parola')->type('password') !!}
        {!! Form::submit('Modifica') !!}
        {!! Form::anchor('Inapoi', 'admin/users/' . $user->id . '/edit') !!}
    {!! Form::close() !!}
@endsection