@extends('adminlte::page')

@section('content')
    {!! Form::open()->put()->route('admin.users.update', [$user->id])->fill($user) !!}
        {!! Form::text('name', 'Nume') !!}
        {!! Form::text('email', 'Email') !!}
        {!! Form::submit('Modifica') !!}
        {!! Form::anchor('Inapoi', '/admin/users') !!}
        {!! Form::anchor('Schimba parola', '/admin/users/'. $user->id . '/password') !!}
    {!! Form::close() !!}
@endsection