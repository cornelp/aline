@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            {!! Form::anchor('Adauga un user nou', '/admin/users/create') !!}
        @endslot

        @if ($users->count())
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nume</th>
                        <th>Email</th>
                        <th>Creat la</th>
                        <th>Modificat la</th>
                        <th>Actiuni</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($users as $user)
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->created_at->format('d.m.Y H:i:s') }}</td>
                            <td>{{ $user->updated_at->format('d.m.Y H:i:s') }}</td>
                            <td>
                                <a href="/admin/users/{{ $user->id }}/edit" class="btn btn-xs">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $users->links() }}
        @else
            Nu exista inregistrari
        @endif
    @endbox
@endsection

@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
@endpush

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script>
        $(function () {
            $('.datepicker').datepicker({
                format: 'dd.mm.yyyy',
                autoclose: true
            })
        });
    </script>
@endpush
