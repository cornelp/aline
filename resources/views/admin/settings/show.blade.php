@extends('adminlte::page')

@section('content')
    {!! Form::open()->put()->route('admin.settings.update', [1])->fill($settings) !!}
        {!! Form::text('address', 'Adresa') !!}
        {!! Form::text('phone', 'Numar de telefon') !!}
        {!! Form::text('quick_schedule', 'Program rapid') !!}
        {!! Form::textarea('description', 'Descriere') !!}
        {!! Form::text('email', 'Email') !!}
        {!! Form::submit('Salveaza') !!}
    {!! Form::close() !!}
@endsection
