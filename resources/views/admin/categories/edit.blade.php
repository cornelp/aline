@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            Editeaza culoare #{{ $category->id }}
        @endslot

        {!! Form::open()->put()->route('admin.categories.update', [$category->id])->fill($category) !!}
            {!! Form::text('name', 'Nume') !!}
            {!! Form::select('is_active', 'E activ', [1 => 'Da', 0 => 'Nu']) !!}
            {!! Form::submit('Modifica') !!}
            {!! Form::anchor('Inapoi')->route('admin.categories.index') !!}

        {!! Form::close() !!}
    @endbox
@endsection
