@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            <a href="{{ route('admin.categories.index') }}" class="btn btn-default">
                <i class="fa fa-backward"></i>
                Inapoi
            </a>
        @endslot

        {!! Form::open()->route('admin.categories.store') !!}
            {!! Form::text('name', 'Nume') !!}
            {!! Form::select('is_active', 'E activ', [1 => 'Da', 0 => 'Nu'], 1) !!}
            {!! Form::submit('Salveaza')->success() !!}
        {!! Form::close() !!}
    @endbox
@endsection
