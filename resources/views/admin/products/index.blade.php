@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            <div class="col-md-6">
                @add(['route' => 'admin.products.create', 'name' => 'produs'])
                @endadd
            </div>

            <div class="col-md-6">
                @search(['route' => 'admin.products.index'])
                @endsearch
            </div>
        @endslot

        @if ($products->count())
            <table class="table table-bordered table-hover">
                @theader(['columns' => [
                    'id' => 'Id',
                    'name' => 'Nume',
                    'category_id' => 'Categorie',
                    'price' => 'Pret',
                    'is_new' => 'E nou',
                    'is_active' => 'E activ',
                    'discount' => 'Discount',
                    'created_at' => 'Creat la',
                    'updated_at' => 'Modificat la',
                    'none' => 'Actiuni'
                ]])
                @endtheader

                <tbody>
                    @foreach ($products as $product)
                        <tr>
                            <td>{{ $product->id }}</td>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->category->name }}</td>
                            <td>{{ $product->price }}</td>
                            <td>{{ $product->is_new ? 'Da' : 'Nu' }}</td>
                            <td>{{ $product->is_active ? 'Da' : 'Nu' }}</td>
                            <td>{{ $product->discount }} %</td>
                            <td>{{ $product->created_at->format('d.m.Y H:i:s') }}</td>
                            <td>{{ $product->updated_at->format('d.m.Y H:i:s') }}</td>
                            <td>
                                <a href="{{ route('admin.products.edit', $product->id) }}" class="btn btn-xs" data-toggle="tooltip" data-placement="top" title="Modifica produs">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a href="{{ route('admin.products.pictures.index', $product->id) }}" class="btn btn-xs" data-toggle="tooltip" data-placement="top" title="Poze produs">
                                    <i class="fa fa-picture-o"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $products->links() }}
        @else
            Nu exista inregistrari
        @endif
    @endbox
@endsection

@section('js')
@endsection
