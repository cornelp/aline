@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            <a href="{{ route('admin.products.index') }}" class="btn btn-default">
                <i class="fa fa-backward"></i>
                Inapoi
            </a>
        @endslot

        {!! Form::open()->route('admin.products.store') !!}
            {!! Form::text('name', 'Nume') !!}
            {!! Form::select('category_id', 'Categorie', $categories) !!}
            {!! Form::textarea('description', 'Descriere') !!}
            {!! Form::text('price', 'Pret') !!}
            {!! Form::select('is_new', 'E nou', [1 => 'Da', 0 => 'Nu'], 1) !!}
            {!! Form::select('is_active', 'E activ', [1 => 'Da', 0 => 'Nu'], 1) !!}
            {!! Form::text('discount', 'Discount (%)', 0)->placeholder('Procent (%)') !!}
            {!! Form::submit('Salveaza')->success() !!}
        {!! Form::close() !!}
    @endbox
@endsection

@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/summernote-bs4.css">
@endpush

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/summernote-bs4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/lang/summernote-ro-RO.min.js"></script>
    <script>
        $(function () {
            $('#description').summernote();
        });
    </script>
@endpush
