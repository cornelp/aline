@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            Editeaza produs #{{ $product->id }}
        @endslot

        {!! Form::open()->put()->route('admin.products.update', [$product->id])->fill($product) !!}
            {!! Form::text('name', 'Nume') !!}
            {!! Form::select('category_id', 'Categorie', $categories) !!}
            {!! Form::textarea('description', 'Descriere') !!}
            {!! Form::text('price', 'Pret') !!}
            {!! Form::select('is_new', 'E nou', [0 => 'Nu', 1 => 'Da']) !!}
            {!! Form::select('is_active', 'E activ', [0 => 'Nu', 1 => 'Da']) !!}
            {!! Form::text('discount', 'Discount (%)')->type('number') !!}
            {!! Form::submit('Modifica') !!}
            {!! Form::anchor('Poze')->route('admin.products.pictures.index', [$product->id])->attrs(['class' => 'success']) !!}
            {!! Form::anchor('Inapoi')->route('admin.products.index')->attrs(['class' => 'warning']) !!}

        {!! Form::close() !!}
    @endbox
@endsection

@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/summernote-bs4.css">
@endpush

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/summernote-bs4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/lang/summernote-ro-RO.min.js"></script>
    <script>
        $(function () {
            $('#description').summernote();
        });
    </script>
@endpush
