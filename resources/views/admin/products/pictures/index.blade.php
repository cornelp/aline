@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            Poze pentru produs #{{ $product->id }}
        @endslot

        {!! Form::open()->multipart()->route('admin.products.pictures.store', [$product->id]) !!}
            {!! Form::file('picture', 'Poza noua') !!}
            {!! Form::text('alt', 'Descriere poza') !!}
            {!! Form::submit('Salveaza poza') !!}
            {!! Form::anchor('Inapoi la produse')->route('admin.products.index') !!}
        {!! Form::close() !!}
    @endbox

    <div class="d-none">
        {!! Form::open()->delete()->route('admin.products.pictures.destroy', [$product->id, 0]) !!}
        {!! Form::close() !!}
    </div>

    @if ($pictures->count())
        @box
            <div class="col-12">
                @foreach ($pictures as $picture)
                    <div class="card">
                        <img class="card-img-top img-thumbnail" src="{{ asset('images/' . $picture->name) }}" alt="{{ $picture->alt }}">
                        <div class="card-body">
                            <p class="card-text">{{ $picture->alt }}</p>
                            <a href="#" onclick="deletePicture(event, {{ $picture->id }})">Sterge</a>
                        </div>
                    </div>
                @endforeach
            </div>
        @endbox
    @endif
@endsection

@push('js')
    <script>
        let deletePicture = function (event, id) {
            let form = $('.d-none form');

            form.attr('action', form.attr('action').replace(0, id));
            form.submit();

            event.preventDefault();
        };
    </script>
@endpush
