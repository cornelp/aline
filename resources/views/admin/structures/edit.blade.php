@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            Editeaza culoare #{{ $structure->id }}
        @endslot

        {!! Form::open()->put()->route('admin.structures.update', [$structure->id])->fill($structure) !!}
            {!! Form::text('name', 'Nume') !!}
            {!! Form::select('is_active', 'Nume', [0 => 'Nu', 1 => 'Da']) !!}
            {!! Form::submit('Modifica') !!}
            {!! Form::anchor('Inapoi')->route('admin.structures.index') !!}

        {!! Form::close() !!}
    @endbox
@endsection
