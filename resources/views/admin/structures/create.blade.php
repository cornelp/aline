@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            <a href="{{ route('admin.structures.index') }}" class="btn btn-default">
                <i class="fa fa-backward"></i>
                Inapoi
            </a>
        @endslot

        {!! Form::open()->route('admin.structures.store') !!}
            {!! Form::text('name', 'Nume') !!}
            {!! Form::select('is_active', 'Nume', [0 => 'Nu', 1 => 'Da'], 1) !!}
            {!! Form::submit('Salveaza')->success() !!}
        {!! Form::close() !!}
    @endbox
@endsection
