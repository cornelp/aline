@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            <div class="col-md-8">
                @add(['route' => 'admin.structures.create', 'name' => 'categorie'])
                @endadd
            </div>

            <div class="col-md-4">
                @search(['route' => 'admin.structures.index'])
                @endsearch
            </div>
        @endslot

        @if ($structures->count())
            <table class="table table-bordered table-hover">
                @theader(['columns' => [
                    'id' => 'Id',
                    'name' => 'Nume',
                    'is_active' => 'E activ',
                    'created_at' => 'Creat la',
                    'updated_at' => 'Modificat la',
                    'none' => 'Actiuni'
                ]])
                @endtheader

                <tbody>
                    @foreach ($structures as $structure)
                        <tr>
                            <td>{{ $structure->id }}</td>
                            <td>{{ $structure->name }}</td>
                            <td>{{ $structure->is_active ? 'Da' : 'Nu' }}</td>
                            <td>{{ $structure->created_at->format('d.m.Y H:i:s') }}</td>
                            <td>{{ $structure->updated_at->format('d.m.Y H:i:s') }}</td>
                            <td>
                                <a href="{{ route('admin.structures.edit', $structure->id) }}" class="btn btn-xs">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $structures->links() }}
        @else
            Nu exista inregistrari
        @endif
    @endbox
@endsection

@section('js')
@endsection
