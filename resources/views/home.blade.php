@extends('layouts.master')

@section('content')
<div class="page-container">
    <div class="top-header top-bg-parallax">
        <div data-parallax="scroll" data-image-src="/images/backgr.png" class="slides parallax-window">
            <div class="slide-content slide-layout-02">
                <div class="container">
                    <div class="slide-content-inner"><img src="/images/slider/slider2-icon.png" data-ani-in="fadeInUp" data-ani-out="fadeOutDown" data-ani-delay="500" alt="aline" class="slide-icon img img-responsive animated">
                        <h3 data-ani-in="fadeInUp" data-ani-out="fadeOutDown" data-ani-delay="1000" class="slide-title animated">ALINE</h3>
                        <p data-ani-in="fadeInUp" data-ani-out="fadeOutDown" data-ani-delay="1500" class="slide-sub-title animated"><span class="line-before"></span><span class="line-after"></span><span class="text">Cake Boutique </span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content-wrapper">
        <section class="about-us-session padding-top-100 padding-bottom-100">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12"><img src="/images/pages/3pic.png" alt="" class="img img-responsive wow zoomIn"></div>
                    <div class="col-md-8 col-sm-12">
                        <div class="swin-sc swin-sc-title style-4 margin-bottom-20 margin-top-50">
                            <p class="top-title"><span>Sa ne cunoastem</span></p>
                            <h3 class="title">Povestea noastra</h3>
                        </div>
                        <p class="des font-bold text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
                        <p class="des margin-bottom-20 text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis ullam laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <div class="swin-btn-wrap center"><a href="#" class="swin-btn center form-submit btn-transparent"> <span>	Despre noi</span></a></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="team-section padding-top-100 padding-bottom-100">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="swin-sc swin-sc-title">
                            <p class="top-title"><span>Echipa Noastra</span></p>
                            <h3 class="title">Va indulcim</h3>
                        </div>
                        <div class="swin-sc swin-sc-team-slider">
                            <div class="team-item swin-transition wow fadeInLeft">
                                <div class="team-img-wrap">
                                    <div class="team-img"><img src="/images/team/team-1.png" alt="" class="img img-responsive"></div>
                                </div>
                                <p class="team-name">MICHAEL DOE</p>
                                <hr>
                            </div>
                            <div class="team-item swin-transition wow fadeInUp">
                                <div class="team-img-wrap">
                                    <div class="team-img"><img src="/images/team/team-2.png" alt="" class="img img-responsive"></div>
                                </div>
                                <p class="team-name">Teresa Doe</p>
                                <hr>
                            </div>
                            <div class="team-item swin-transition wow fadeInRight">
                                <div class="team-img-wrap">
                                    <div class="team-img"><img src="/images/team/team-3.png" alt="" class="img img-responsive"></div>
                                </div>
                                <p class="team-name">BENJAMIN MARK</p>
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="servicii" class="service-section-02 padding-top-100 padding-bottom-100">
            <div class="container">
                <div class="swin-sc swin-sc-title">
                    <p class="top-title"><span>Serviciile noastre</span></p>
                    <h3 class="title">Ce va oferim</h3>
                </div>
                <div class="swin-sc swin-sc-iconbox">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="item icon-box-02 wow fadeInUpShort">
                                <div class="wrapper-icon"><i class="icons swin-icon-dish"></i><span class="number">1</span></div>
                                <h4 class="title">Aranjament Candy Bar</h4>
                                <div class="description">Lorem ipsum dolor sit amet, tong consecteturto sed eiusmod incididunt utote labore et</div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div data-wow-delay="0.5s" class="item icon-box-02 wow fadeInUpShort">
                                <div class="wrapper-icon"><i class="icons swin-icon-delivery"></i><span class="number">2</span></div>
                                <h4 class="title">Transport Frigorific</h4>
                                <div class="description">Lorem ipsum dolor sit amet, tong consecteturto sed eiusmod incididunt utote labore et</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@endsection
