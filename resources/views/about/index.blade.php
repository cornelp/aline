@extends('layouts.master')

@section('content')

<div class="page-container">
    <div data-bottom-top="background-position: 50% 50px;" data-center="background-position: 50% 0px;" data-top-bottom="background-position: 50% -50px;" class="page-title page-about">
        <div class="container">
            <div class="title-wrapper">
            </div>
        </div>
    </div>
    <div class="page-content-wrapper">
        <section class="ab-timeline-section padding-top-100 padding-bottom-100">
            <div class="container">
                <div class="swin-sc swin-sc-title style-2">
                    <h3 class="title"><span>Despre noi</span></h3>
                </div>
                <div data-item="6" class="swin-sc swin-sc-timeline-2">
                    <div class="nav-slider">
                        <div class="slides">
                            <div class="timeline-content-item">
                                <div class="timeline-content-detail">
                                    @include('about.content')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@endsection
