@extends('layouts.master')

@section('content')
<div class="page-container">
    <div data-bottom-top="background-position: 50% 50px;" data-center="background-position: 50% 0px;" data-top-bottom="background-position: 50% -50px;" class="page-title page-about">
        <div class="container">
            <div class="title-wrapper">
            </div>
        </div>
    </div>
    <div class="page-content-wrapper">
        <section class="ab-timeline-section padding-top-100 padding-bottom-100">
            <div class="container">
                <div class="swin-sc swin-sc-title style-2">
                    <h3 class="title"><span>Comanda</span></h3>
                </div>
                <div data-item="6" class="swin-sc swin-sc-timeline-2">
                    <div class="nav-slider">
                        <div class="slides">
                            <div class="timeline-content-item">
                                <p class="timeline-heading">Completati formularul de comanda de mai jos cu produsele dorite. De retinut este faptul ca odata comanda trimisa aceasta nu este valida pana ce nu veti fi sunat pentru confirmarea acesteia.</p>

                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                <div class="reservation-form style-02">
                                    <div class="swin-sc swin-sc-contact-form light mtl style-full">
                                        <form method="POST" action="{{ route('orders.store') }}">
                                            @csrf

                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                                    <input type="text" name="name" placeholder="Nume" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                                    <input type="text" name="email" placeholder="Email" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <div class="fa fa-calendar"></div>
                                                    </div>
                                                    <input type="text" name="delivery_date" placeholder="Cand sa fie gata comanda" class="datepicker form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <div class="fa fa-map"></div>
                                                    </div>
                                                    <input type="text" name="address" placeholder="Adresa" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <div class="fa fa-phone"></div>
                                                    </div>
                                                    <input type="text" name="phone" placeholder="Telefon" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <textarea placeholder="Mesaj" name="description" class="form-control" required></textarea>
                                            </div>
                                            <div class="form-group">
                                                <div class="swin-btn-wrap">
                                                    <button type="submit" class="swin-btn center form-submit">
                                                        <span>Trimite</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection

@include('components.datepicker')