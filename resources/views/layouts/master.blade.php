<!DOCTYPE html>
<html lang="en" itemscope itemtype="http://schema.org/WebPage">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta property="og:type" content="website" />
	<meta property="og:url" content="https://aline.ro" />
	<meta property="og:title" content="Aline Cake Boutique" />
	<meta property="og:site_name" content="Aline Cake Boutique" />
	<meta property="og:description" content="Aline Cake Boutique" />
	<meta property="og:image" content="#" />
    <title>Aline</title>
    <!-- Bootstrap CSS-->
    <link href="/vendors/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome-->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries-->
    <!-- WARNING: Respond.js doesn't work if you view the page via file://-->
    <!-- IE 9-->
    <!-- Vendors-->
    <link rel="stylesheet" href="/vendors/flexslider/flexslider.min.css">
    <link rel="stylesheet" href="/vendors/swipebox/css/swipebox.min.css">
    <link rel="stylesheet" href="/vendors/slick/slick.min.css">
    <link rel="stylesheet" href="/vendors/slick/slick-theme.min.css">
    <link rel="stylesheet" href="/vendors/animate.min.css">
    <link rel="stylesheet" href="/vendors/bootstrap-datepicker/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="/vendors/pageloading/css/component.min.css">
    <!-- Font-icon-->
    <link rel="stylesheet" href="/fonts/font-icon/style.css">
    <!-- Style-->
    <link rel="stylesheet" type="text/css" href="/css/layout.css">
    <link rel="stylesheet" type="text/css" href="/css/elements.css">
    <link rel="stylesheet" type="text/css" href="/css/extra.css">
    <link rel="stylesheet" type="text/css" href="/css/widget.css">
    <link id="colorpattern" rel="stylesheet" type="text/css" href="/css/color/colordefault.css">
    <link rel="stylesheet" type="text/css" href="/css/responsive.css">
    <link rel="stylesheet" type="text/css" href="/css/live-settings.css">
    <!-- Google Font-->
    <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Rancho" rel="stylesheet">

    @stack('css')

    <!-- Script Loading Page-->
    <script src="/vendors/html5shiv.js"></script>
    <script src="/vendors/respond.min.js"></script>
    <script src="/vendors/pageloading/js/snap.svg-min.js"></script>
    <script src="/vendors/pageloading/sidebartransition/js/modernizr.custom.js"></script>
</head>
<body class="layout-boxed">
    <div id="pagewrap" class="pagewrap">
        <div id="html-content" class="wrapper-content">
            <header class="header-transparent">
                <div class="header-top top-layout-02">
                    <div class="container">
                        <div class="topbar-left">
                            <div class="topbar-content">
                                <div class="item">
                                    <div class="wg-contact"><i class="fa fa-map-marker"></i><span>{{ $settings->address }}</span></div>
                                </div>
                                <div class="item">
                                    <div class="wg-contact"><i class="fa fa-phone"></i><span>{{ $settings->phone }}</span></div>
                                </div>
                                <div class="item">
                                    <div class="wg-contact"><i class="fa fa-calendar"></i><span>{{  $settings->quick_schedule }}</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="topbar-right">
                            <div class="topbar-content">
                                <div class="item">
                                    <ul class="socials-nb list-inline wg-social">
                                        <li><a href="https://www.facebook.com/ALINEcakeboutique/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-main">
                    <div class="container">
                        <div class="open-offcanvas">&#9776;</div>
                        <div class="utility-nav">
                        </div>
                        <div class="header-logo"><a href="/" class="logo logo-static"><img src="/images/aline.png" alt="aline" class="logo-img"></a><a href="/" class="logo logo-fixed"><img src="/images/logo.png" alt="aline" class="logo-img"></a></div>
                        <nav id="main-nav-offcanvas" class="main-nav-wrapper">
                            <div class="close-offcanvas-wrapper"><span class="close-offcanvas">x</span></div>
                            <div class="main-nav">
                                <ul id="main-nav" class="nav nav-pills">
                                    <li class="dropdown current-menu-item"><a href="/" class="dropdown-toggle">Acasa</a></li>
                                    <li><a href="/about-us">Despre Noi</a></li>
                                    <li class="dropdown"><a> Categorii</a>
                                        @if ($categories && $categories->count())
                                        <ul class="dropdown-menu">
                                            @foreach ($categories as $category)
                                            <li><a href="/{{ $category->slug }}">{{ $category->name }}</a></li>
                                            @endforeach
                                        </ul>
                                        @endif
                                    </li>
                                    <li><a href="/services">Servicii</a></li>
                                    <li><a href="/catalog">Cataloage</a></li>
                                    <li><a href="/order">Comanda</a></li>
                                    <li><a href="/contact">Contact</a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </header>

            @yield('content')

            <footer>
                <div class="footer-top"></div>
                <div class="footer-main">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="ft-widget-area">
                                    <div class="ft-area1">
                                        <div class="swin-wget swin-wget-about">
                                            <div class="clearfix"><a class="wget-logo"><img src="/images/aline.png" alt="" class="img img-responsive"></a>
                                                <ul class="socials socials-about list-unstyled list-inline">
                                                    <li><a href="https://www.facebook.com/ALINEcakeboutique/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                                </ul>
                                            </div>
                                            <div class="wget-about-content">
                                                <p>{{ $settings->description }}</p>
                                            </div>
                                            <div class="about-contact-info clearfix">
                                                <div class="address-info">
                                                    <div class="info-icon"><i class="fa fa-map-marker"></i></div>
                                                    <div class="info-content">
                                                        <p>{{ $settings->address }}</p>
                                                    </div>
                                                </div>
                                                <div class="phone-info">
                                                    <div class="info-icon"><i class="fa fa-mobile-phone"></i></div>
                                                    <div class="info-content">
                                                        <p>{{ $settings->phone }}</p>
                                                    </div>
                                                </div>
                                                <div class="email-info">
                                                    <div class="info-icon"><i class="fa fa-envelope"></i></div>
                                                    <div class="info-content">
                                                        <p>{{ $settings->email }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="email-info">
                                                <div class="info-icon"><i class="fa fa-web"></i></div>
                                                <div class="info-content">
                                                    <p><a href="/terms" target="_blank">Termeni si Conditii </a></p>
                                                </div>
                                            </div>
                                            <div class="email-info">
                                                <div class="info-icon"><i class="fa fa-web"></i></div>
                                                <div class="info-content">
                                                    <p> Site creeat de <a href="https://xita.ro" target="_blank">xitaro </a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="ft-fixed-area">
                                    <div class="reservation-box">
                                        <div class="reservation-wrap">
                                            <h3 class="res-title">Program</h3>
                                            <div class="res-date-time">
                                                <div class="res-date-time-item">
                                                    <div class="res-date">
                                                        <div class="res-date-item">
                                                            <div class="res-date-text">
                                                                <p>Luni:</p>
                                                            </div>
                                                            <div class="res-date-dot">.......................................</div>
                                                        </div>
                                                    </div>
                                                    <div class="res-time">
                                                        <div class="res-time-item">
                                                            <p>9:00 - 19:00</p>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="res-date-time-item">
                                                    <div class="res-date">
                                                        <div class="res-date-item">
                                                            <div class="res-date-text">
                                                                <p>Marti:</p>
                                                            </div>
                                                            <div class="res-date-dot">.......................................</div>
                                                        </div>
                                                    </div>
                                                    <div class="res-time">
                                                        <div class="res-time-item">
                                                            <p>9:00 - 19:00</p>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="res-date-time-item">
                                                    <div class="res-date">
                                                        <div class="res-date-item">
                                                            <div class="res-date-text">
                                                                <p>Miercuri:</p>
                                                            </div>
                                                            <div class="res-date-dot">.......................................</div>
                                                        </div>
                                                    </div>
                                                    <div class="res-time">
                                                        <div class="res-time-item">
                                                            <p>9:00 - 19:00</p>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="res-date-time-item">
                                                    <div class="res-date">
                                                        <div class="res-date-item">
                                                            <div class="res-date-text">
                                                                <p>Joi:</p>
                                                            </div>
                                                            <div class="res-date-dot">.......................................</div>
                                                        </div>
                                                    </div>
                                                    <div class="res-time">
                                                        <div class="res-time-item">
                                                            <p>9:00 - 19:00</p>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="res-date-time-item">
                                                    <div class="res-date">
                                                        <div class="res-date-item">
                                                            <div class="res-date-text">
                                                                <p>Vineri:</p>
                                                            </div>
                                                            <div class="res-date-dot">.......................................</div>
                                                        </div>
                                                    </div>
                                                    <div class="res-time">
                                                        <div class="res-time-item">
                                                            <p>9:00 - 19:00</p>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="res-date-time-item">
                                                    <div class="res-date">
                                                        <div class="res-date-item">
                                                            <div class="res-date-text">
                                                                <p>Sambata:</p>
                                                            </div>
                                                            <div class="res-date-dot">.......................................</div>
                                                        </div>
                                                    </div>
                                                    <div class="res-time">
                                                        <div class="res-time-item">
                                                            <p>9:00 - 19:00</p>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="res-date-time-item">
                                                    <div class="res-date">
                                                        <div class="res-date-item">
                                                            <div class="res-date-text">
                                                                <p>Duminica:</p>
                                                            </div>
                                                            <div class="res-date-dot">.......................................</div>
                                                        </div>
                                                    </div>
                                                    <div class="res-time">
                                                        <div class="res-time-item">
                                                            <p>9:00 - 13:00</p>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <h3 class="res-title">Contact</h3>
                                            <p class="res-number">0747 372 376</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer><a id="totop" href="#" class="animated"><i class="fa fa-angle-double-up"></i></a>
        </div>
        <div id="loader" data-opening="m -5,-5 0,70 90,0 0,-70 z m 5,35 c 0,0 15,20 40,0 25,-20 40,0 40,0 l 0,0 C 80,30 65,10 40,30 15,50 0,30 0,30 z" class="pageload-overlay">
            <div class="loader-wrapper">
                <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewbox="0 0 80 60" preserveaspectratio="none">
                    <path d="m -5,-5 0,70 90,0 0,-70 z m 5,5 c 0,0 7.9843788,0 40,0 35,0 40,0 40,0 l 0,60 c 0,0 -3.944487,0 -40,0 -30,0 -40,0 -40,0 z"></path>
                </svg>
                <div class="sk-circle">
                    <div class="sk-circle1 sk-child"></div>
                    <div class="sk-circle2 sk-child"></div>
                    <div class="sk-circle3 sk-child"></div>
                    <div class="sk-circle4 sk-child"></div>
                    <div class="sk-circle5 sk-child"></div>
                    <div class="sk-circle6 sk-child"></div>
                    <div class="sk-circle7 sk-child"></div>
                    <div class="sk-circle8 sk-child"></div>
                    <div class="sk-circle9 sk-child"></div>
                    <div class="sk-circle10 sk-child"></div>
                    <div class="sk-circle11 sk-child"></div>
                    <div class="sk-circle12 sk-child"></div>
                </div>
                <div class="sk-circle sk-circle-out">
                    <div class="sk-circle1 sk-child"></div>
                    <div class="sk-circle2 sk-child"></div>
                    <div class="sk-circle3 sk-child"></div>
                    <div class="sk-circle4 sk-child"></div>
                    <div class="sk-circle5 sk-child"></div>
                    <div class="sk-circle6 sk-child"></div>
                    <div class="sk-circle7 sk-child"></div>
                    <div class="sk-circle8 sk-child"></div>
                    <div class="sk-circle9 sk-child"></div>
                    <div class="sk-circle10 sk-child"></div>
                    <div class="sk-circle11 sk-child"></div>
                    <div class="sk-circle12 sk-child"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery-->
    <script src="/vendors/jquery-1.10.2.min.js"></script>
    <!-- Bootstrap JavaScript-->
    <script src="/vendors/bootstrap/js/bootstrap.min.js"></script>
    <!-- Vendors-->
    <script src="/vendors/flexslider/jquery.flexslider-min.js"></script>
    <script src="/vendors/swipebox/js/jquery.swipebox.min.js"></script>
    <script src="/vendors/slick/slick.min.js"></script>
    <script src="/vendors/isotope/isotope.pkgd.min.js"></script>
    <script src="/vendors/jquery-countTo/jquery.countTo.min.js"></script>
    <script src="/vendors/jquery-appear/jquery.appear.min.js"></script>
    <script src="/vendors/parallax/parallax.min.js"></script>
    <script src="/vendors/gmaps/gmaps.min.js"></script>
    <script src="/vendors/audiojs/audio.min.js"></script>
    <script src="/vendors/vide/jquery.vide.min.js"></script>
    <script src="/vendors/pageloading/js/svgLoader.min.js"></script>
    <script src="/vendors/pageloading/js/classie.min.js"></script>
    <script src="/vendors/pageloading/sidebartransition/js/sidebarEffects.min.js"></script>
    <script src="/vendors/nicescroll/jquery.nicescroll.min.js"></script>
    <script src="/vendors/wowjs/wow.min.js"></script>
    <script src="/vendors/skrollr.min.js"></script>
    <script src="/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="/vendors/jquery-cookie/js.cookie.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js" integrity="sha384-mE6eXfrb8jxl0rzJDBRanYqgBxtJ6Unn4/1F7q4xRRyIw7Vdg9jP4ycT7x1iVsgb" crossorigin="anonymous"></script>
    <!-- Own script-->
    <script src="/js/layout.js"></script>
    <script src="/js/elements.js"></script>
    <script src="/js/widget.js"></script>

    @stack('js')
</body>
</html>
