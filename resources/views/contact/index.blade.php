@extends('layouts.master')

@section('content')
<div class="page-container">
    <div data-bottom-top="background-position: 50% 50px;" data-center="background-position: 50% 0px;" data-top-bottom="background-position: 50% -50px;" class="page-title page-contact">
        <div class="container">
            <div class="title-wrapper">
            </div>
        </div>
    </div>
    <div class="page-content-wrapper">
        <section class="ct-info-section padding-top-100 padding-bottom-100">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-12">
                        <div class="swin-sc swin-sc-title style-2 text-left">
                            <p class="title"><span>Contact</span></p>
                        </div>

                        @if ($errors->any())
                        <div class="box-body">
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @endif

                        <div class="reservation-form style-02">
                            <div class="swin-sc swin-sc-contact-form light mtl style-full">
                                <form method="POST" action="{{ route('contact.post') }}">
                                    @csrf

                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                            <input type="text" placeholder="Nume" class="form-control" name="name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                            <input type="text" placeholder="Email" class="form-control" name="email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <div class="fa fa-phone"></div>
                                            </div>
                                            <input type="text" placeholder="Telefon" class="form-control" name="phone">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea placeholder="Mesaj" class="form-control" name="message"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <div class="swin-btn-wrap">
                                            <button type="submit" class="swin-btn center form-submit">
                                                <span>Trimite</span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="swin-sc swin-sc-title style-2 text-left">
                            <p class="title"><span>Informatii de contact</span></p>
                        </div>
                        <div class="swin-sc swin-sc-contact">
                            <div class="media item">
                                <div class="media-left">
                                    <div class="wrapper-icon"><i class="icons fa fa-map-marker"></i></div>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading title">Aline Cake Boutique</h4>
                                    <div class="description">{{ $settings->address }}</div>
                                </div>
                            </div>
                            <div class="media item">
                                <div class="media-left">
                                    <div class="wrapper-icon"><i class="icons fa fa-phone"></i></div>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading title">Telefon</h4>
                                    <div class="description">{{ $settings->phone }}</div>
                                </div>
                            </div>
                            <div class="media item">
                                <div class="media-left">
                                    <div class="wrapper-icon"><i class="icons fa fa-envelope"></i></div>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading title">Mail</h4>
                                    <div class="description">
                                        <p>
                                            <a href="mailto:{{ $settings->email }}">
                                                {{ $settings->email }}</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    @endsection
