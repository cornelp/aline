@component('mail::message')
Buna ziua,

<p>Am primit un nou contact de la {{ $data['name'] }}, email {{ $data['email'] }}, telefon {{ $data['phone'] ?? '-' }} cu mesajul: </p>
<p>{{ $data['message'] }}</p>

Multumesc,<br>
{{ config('app.name') }}
@endcomponent
