@extends('layouts.master')

@section('content')
<div class="page-container">
    <div data-bottom-top="background-position: 50% 50px;" data-center="background-position: 50% 0px;" data-top-bottom="background-position: 50% -50px;" class="page-title page-product">
        <div class="container">
            <div class="title-wrapper">
            </div>
        </div>
    </div>
    <div class="page-content-wrapper">
        <div class="container">
            <section class="product-single padding-top-100 padding-bottom-100">
                <div class="row">
                    <div class="col-md-5">
                        <div class="product-image">
                            <div class="product-featured-image">
                                @if ($product)
                                    <div class="main-slider">
                                        <div class="slides">
                                            @foreach ($product->pictures as $picture)
                                                <div class="featured-image-item">
                                                    <img src="/images/{{ $picture->name }}" alt="{{ $picture->alt ?? '' }}" class="img img-responsive">
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="product-summary">
                            <div class="product-title">
                                <div class="title">{{ $product->name }}</div>
                            </div>
                            <div class="product-desc">
                                <p>{!! $product->description !!}</p>
                            </div>
                            <div class="product-share"><span class="caption">Share</span>
                                <ul class="socials list-unstyled list-inline">
                                    <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection
