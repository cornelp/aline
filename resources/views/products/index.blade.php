@extends('layouts.master')

@section('content')
<div class="page-container">
    <div data-bottom-top="background-position: 50% 50px;" data-center="background-position: 50% 0px;" data-top-bottom="background-position: 50% -50px;" class="page-title page-menu">
        <div class="container">
            <div class="title-wrapper">
            </div>
        </div>
    </div>
    <div class="page-content-wrapper">
        <section class="product-sesction-02 padding-top-100 padding-bottom-100">
            <div class="container">
                <div class="swin-sc swin-sc-title style-3">
                    <p class="title"><span>Produse in categoria "{{ $category->name }}"</span></p>
                </div>
                <div class="swin-sc swin-sc-product products-02">
                    <div class="products">
                        <div class="row slick-padding">
                            @foreach ($category->products as $product)
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="blog-item item swin-transition">
                                        <div class="block-img"><img src="/images/{{ $product->getFirstPicture()->name }}" alt="" class="img img-responsive">
                                        </div>
                                        <div class="block-content">
                                            <h5 class="title"><a href="/{{ $category->slug }}/{{ $product->slug }}">{{ $product->name }}</a></h5>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@endsection
