<thead>
    <tr>
        @foreach ($columns as $name => $column)
            <th>
                @if ($name == 'none')
                    {{ $column }}
                @else
                    <a href="#" onclick="sortBy(event, '{{ $name }}')">
                        {{ $column }}</th>
                    </a>
                @endif
        @endforeach
    </tr>
</thead>

@push('js')
    <script>
        let sortBy = function (event, name) {
            // get old value for sort
            let oldSort = $('#sort').val();
            let oldOrder = $('#order').val();

            // if old value is the same, change asc to desc
            $('#order').val(oldSort == name && oldOrder == 'asc' ? 'desc' : 'asc');

            $('#sort').val(name).trigger('change');

            event.preventDefault();
        };
    </script>
@endpush