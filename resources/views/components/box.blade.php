<div class="box">
    <div class="box-header">
        {{ $buttons ?? '' }}
    </div>

    {{-- @if ($errors->any())
        <div class="box-body">
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif --}}

    <div class="box-body">
        {{ $slot }}
    </div>
</div>
