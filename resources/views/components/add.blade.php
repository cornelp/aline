<a class="btn btn-primary" href="{{ route($route) }}">
    <i class="fa fa-plus"></i>
    Adauga {{ $name }}
</a>