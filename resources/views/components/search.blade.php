{!! Form::open()->get()->route($route)->inlineForm() !!}
    {!! Form::text('search', 'Cauta', request()->search)->placeholder('Cauta')->attrs(['class' => 'pull-right']) !!}
    {!! Form::hidden('sort', request()->sort)->attrs(['onChange' => 'submitSearch()']) !!}
    {!! Form::hidden('order', request()->order ?? 'asc') !!}
{!! Form::close() !!}

@push('js')
    <script>
        let submitSearch = function () {
            $('#search').closest('form').submit();
        };

        $(function () {
            $('#search').on('blur enter', function () {
                submitSearch();
            });
        });
    </script>
@endpush
