<?php

use Illuminate\Database\Seeder;

class InitialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Setting::create([
            'address' => 'Adresa',
            'quick_schedule' => '9-17',
            'description' => 'ceva desc',
            'email' => 'fara email',
            'phone' => '0740001002'
        ]);

        $user = App\User::create([
            'name' => 'Admin',
            'email' => 'admin@aline.ro',
            'password' => bcrypt('admin')
        ]);

        $structure = App\Structure::create(['name' => 'Ciocolata']);

        $category = App\Category::create(['name' => 'Torturi']);

        $product = App\Product::create([
            'category_id' => $category->id,
            'name' => 'Un Prim Tort'
        ]);

        $order = App\Order::create([
            'name' => 'Ion',
            'email' => 'ion@mail.com',
            'address' => 'O adresa oarecare',
            'phone' => '0740610288',
            'delivery_date' => now()->addDay()->format('d.m.Y H:i'),
            'description' => 'vreau un tort - 1.2 kg',
            'observations' => '',
            'created_by' => $user->id
        ]);

        $order->details()->create([
            'product_id' => $product->id,
            'structure_id' => $structure->id,
            'quantity' => 1,
            'price' => 100,
            'text' => 'ceva si aici'
        ]);
    }
}
