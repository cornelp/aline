<?php

namespace App\Http\Controllers\Admin;

use App\Structure;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class StructureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $structures = Structure::search(request('search'))
            ->orderBy(request('sort', 'id'), request('order', 'asc'))
            ->paginate(10);

        return view('admin.structures.index', compact('structures'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.structures.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Structure::create($this->makeValidation($request));

        return redirect()->route('admin.structures.index');
    }

    protected function makeValidation($request)
    {
        return $this->validate($request, [
            'name' => 'required|max:255',
            'is_active' => 'required'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Structure  $Structure
     * @return \Illuminate\Http\Response
     */
    public function show(Structure $structure)
    {
        return redirect()->route('admin.structures.edit', $structure->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Structure  $Structure
     * @return \Illuminate\Http\Response
     */
    public function edit(Structure $structure)
    {
        return view('admin.structures.edit', compact('structure'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AppStructure  $appStructure
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Structure $structure)
    {
        $structure->update($this->makeValidation($request));

        return redirect()->route('admin.structures.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppStructure  $appStructure
     * @return \Illuminate\Http\Response
     */
    public function destroy(Structure $structure)
    {
        //
    }
}
