<?php

namespace App\Http\Controllers\Admin;

use App\OrderDetail;
use App\Order;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Structure;

class OrderDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function index(Order $order)
    {
        $order->load('details');

        return view('admin.orders.details.index', compact('order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function create(Order $order)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Order $order)
    {
        $order->details()->create(
            $this->makeValidation($request)
        );

        return redirect()->route('admin.orders.show', $order->id);
    }

    protected function makeValidation($request)
    {
        return $this->validate($request, [
            'product_id' => 'required|numeric',
            'quantity' => 'required|numeric',
            'structure_id' => 'required|numeric',
            'price' => 'required|numeric',
            'weight' => 'numeric',
            'for_no_people' => 'numeric|nullable',
            'message' => 'nullable',
            'decor' => 'nullable',
            'text' => 'nullable',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @param  \App\OrderDetail  $orderDetail
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order, OrderDetail $orderDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @param  \App\OrderDetail  $orderDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order, $id)
    {
        $products = Product::pluck('name', 'id')->all();
        $structures = Structure::pluck('name', 'id')->all();
        $detail = OrderDetail::find($id);

        return view('admin.orders.details.edit',
            compact('order', 'detail', 'products', 'structures'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @param  \App\OrderDetail  $orderDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order, $id)
    {
        $attrs = $this->validate($request, [
            'product_id' => 'required|numeric',
            'structure_id' => 'required|numeric',
            'quantity' => 'required|numeric',
            'price' => 'required|numeric',
            'weight' => 'required|numeric',
            'for_no_people' => 'numeric',
            'message' => 'nullable',
            'decor' => 'nullable',
            'text' => 'nullable',
        ]);

        OrderDetail::findOrFail($id)->update($attrs);

        return redirect()->route('admin.orders.show', [$order->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @param  \App\OrderDetail  $orderDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order, $id)
    {
        OrderDetail::findOrFail($id)->delete();

        return redirect()->route('admin.orders.show', $order->id);
    }
}
