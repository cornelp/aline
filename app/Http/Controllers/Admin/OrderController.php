<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Structure;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        [$startDate, $endDate] = $this->handleDates(request());

        $orders = Order::with('details', 'owner')
            ->whereBetween('delivery_date', [$startDate, $endDate])
            ->paginate(10);

        return view('admin.orders.index', compact('orders', 'startDate', 'endDate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::pluck('name', 'id')->all();

        return view('admin.orders.create', compact('products'));
    }

    protected function handleDates($request)
    {
        $startDate = now()->parse($request->input('startDate', session('startDate')))->startOfDay();
        $endDate = now()->parse($request->input('endDate', session('endDate')))->endOfDay();

        session(compact('startDate'));
        session(compact('endDate'));

        return [$startDate, $endDate];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attrs = $this->makeValidation($request);

        $order = Order::create($attrs);

        return redirect()->route('admin.orders.show', $order->id);
    }

    protected function makeValidation($request)
    {
        return $this->validate($request, [
            'name' => 'required',
            'email' => 'email|nullable',
            'address' => 'nullable',
            'phone' => 'numeric',
            'delivery_date' => 'required|date_format:d.m.Y H:i',
            'description' => 'max:10000',
            'observations' => 'max:10000',
            'is_processed' => 'boolean',
            'is_canceled' => 'boolean',
            'with_pickup' => 'boolean',
            'advance' => 'numeric',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $structures = Structure::whereIsActive(true)->pluck('name', 'id')->all();
        $products = Product::pluck('name', 'id')->all();

        $order->load('details');

        return view('admin.orders.show', compact('order', 'products', 'structures'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        return view('admin.orders.edit', compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $order->update(
            $this->makeValidation($request)
        );

        if ($request->wantsJson()) {
            return ['success' => 'true'];
        }

        return redirect()->route('admin.orders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function pdfExport(Order $order)
    {
        $pdf = \PDF::loadView('admin.orders.pdf-export', compact('order'));

        return $pdf->download('Bon comanda ' . $order->id . '.pdf');
    }
}
