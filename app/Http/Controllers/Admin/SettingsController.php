<?php

namespace App\Http\Controllers\Admin;

use App\Setting;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    public function edit()
    {
        $settings = Setting::first();

        return view('admin.settings.show', compact('settings'));
    }

    public function update($id)
    {
        Setting::first()->update(request()->all());

        return redirect()->route('admin.settings.edit', [1]);
    }
}
