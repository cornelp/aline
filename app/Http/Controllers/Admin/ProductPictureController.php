<?php

namespace App\Http\Controllers\Admin;

use App\Picture;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductPictureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        $product->load('pictures');
        $pictures = $product->pictures;

        return view('admin.products.pictures.index', compact('pictures', 'product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function create(Product $product)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Product $product)
    {
        $this->validate($request, [
            'picture' => 'required|file'
        ]);

        // save reference to db
        $picture = Picture::fromRequest($request);

        // attach to product
        $product->pictures()->attach($picture->id);

        return redirect()->route('admin.products.pictures.index', $product->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @param  \App\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product, Picture $picture)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @param  \App\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product, Request $request)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @param  \App\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product, Picture $picture)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @param  \App\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product, Picture $picture)
    {
        $picture->delete();

        return redirect()->route('admin.products.pictures.index', $product->id);
    }
}
