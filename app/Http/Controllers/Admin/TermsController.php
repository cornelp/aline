<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class TermsController extends Controller
{
    public function edit()
    {
        $content = view('terms.content')->render();

        return view('admin.terms.edit', compact('content'));
    }

    public function update()
    {
        \File::put(resource_path('views/terms/content.blade.php'), request('content'));

        return redirect('admin/terms');
    }
}
