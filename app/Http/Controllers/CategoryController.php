<?php

namespace App\Http\Controllers;

use App\Category;

class CategoryController extends Controller
{
    public function show($slug)
    {
        $category = Category::with('products.pictures')
            ->whereIsActive(true)
            ->whereSlug($slug)
            ->firstOrFail();

        return view('products.index', compact('category'));
    }
}
