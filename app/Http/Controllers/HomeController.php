<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\ContactMail;

class HomeController extends Controller
{
    public function index()
    {
        return view('home');
    }

    public function about()
    {
        return view('about.index');
    }

    public function showContact()
    {
        return view('contact.index');
    }

    public function sendContact()
    {
        $attrs = $this->validate(request(), [
            'name' => 'required|min:1|max:100',
            'email' => 'required|email',
            'message' => 'required|max:10000',
            'phone' => 'numeric|nullable'
        ]);

        \Mail::to(config('MAIL_FROM_ADDRESS'))->send(new ContactMail($attrs));

        // return (new ContactMail($attrs))->render();

        return redirect()->route('home');
    }

    public function showCatalogs()
    {
        return view('catalog.index');
    }

    public function showOrder()
    {
        return view('order.create');
    }

    public function saveOrder()
    {
        // validate
        $attrs = $this->validate(request(), [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'address' => 'required|max:255',
            'delivery_date' => 'required|date_format:d.m.Y H:i',
            'description' => 'required|max:10000'
        ]);

        // save order
        Order::create($attrs);

        // redirect back
        return redirect()->route('home');
    }

    public function showTerms()
    {
        return view('terms.index');
    }
}
