<?php

namespace App\Http\Controllers;

use App\Product;

class ProductController extends Controller
{
    public function show($categorySlug, $productSlug)
    {
        $product = Product::with('pictures')
            ->whereHas('category', function ($query) use ($categorySlug) {
                $query->whereSlug($categorySlug)
                    ->whereIsActive(true);
            })
            ->whereSlug($productSlug)
            ->first();

        return view('products.show', compact('product'));
    }
}
