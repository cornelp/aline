<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Structure extends Model
{
    use SearchableTrait;

    protected $fillable = ['name', 'is_active'];

    protected $casts = ['is_active' => 'boolean'];

    protected $searchable = [
        'columns' => [
            'name' => 10
        ]
    ];
}
