<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Product extends Model
{
    use SearchableTrait;

    protected $fillable = [
        'is_new', 'discount', 'is_active', 'price',
        'category_id', 'description', 'name', 'slug'
    ];

    protected $casts = [
        'is_new' => 'boolean',
        'is_active' => 'boolean',
    ];

    protected $searchable = [
        'columns' => [
            'name' => 10,
            'description' => 5
        ]
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['slug'] = trim(str_replace(' ', '-', strtolower($value)));
        $this->attributes['name'] = $value;
    }

    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = $value * 100;
    }

    public function getPriceAttribute($value)
    {
        return $value / 100;
    }

    public function pictures()
    {
        return $this->belongsToMany(Picture::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function scopeByCategory($query, $categoryId)
    {
        $query->whereHas('category', function ($query) use ($categoryId) {
            $query->where('id', $categoryId);
        });
    }

    public function getFinalPrice()
    {
        return $this->price - $this->price * $this->discount / 100;
    }

    public function getFirstPicture()
    {
        return optional($this->pictures->first());
    }

    public function scopeActives($query)
    {
        $query->where('is_active', true);
    }
}
