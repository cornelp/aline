<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Picture extends Model
{
    protected $fillable = [
        'name', 'alt'
    ];

    public static function boot()
    {
        parent::boot();

        self::checkImageFolder();
    }

    public static function checkImageFolder()
    {
        $path = public_path('/images');

        if (! File::isDirectory($path)) {
            File::makeDirectory($path);
        }
    }

    public static function fromRequest($request)
    {
        $name = md5(time()) . '.' . $request->picture->getClientOriginalExtension();

        // move picture to images folder
        $request->picture->move(public_path('/images/'), $name);

        return self::create([
            'name' => $name,
            'alt' => request('alt', 'no-description')
        ]);
    }
}
