<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use App\Category;
use App\Setting;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (! $this->app->runningInConsole() && ! request()->is('admin/*')) {
            view()->share('categories', Category::active()->get());
            view()->share('settings', Setting::first());
        }

        Blade::component('components.box', 'box');
        Blade::component('components.search', 'search');
        Blade::component('components.add', 'add');
        Blade::component('components.theader', 'theader');
        Blade::component('components.modal', 'modal');
        Blade::component('components.datepicker', 'datepicker');

        Blade::directive('float', function ($value) {
            return "<?php echo number_format({$value}, 2); ?>";
        });

        // app()->setLocale('ro');

		Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
