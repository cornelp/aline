<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'name', 'email', 'phone', 'delivery_date',
        'description', 'address', 'observations', 'is_processed',
        'is_canceled', 'advance', 'created_by'
    ];

    protected $dates = ['delivery_date'];

    protected $casts = [
        'is_processed' => 'boolean',
        'is_canceled' => 'boolean',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $delivery_date = now()->parse($model->delivery_date ?? now());

            if ($delivery_date->lt(now())) {
                $model->delivery_date = now()->addDay();
            }
        });
    }

    public function getAdvanceAttribute($value)
    {
        return $value / 100;
    }

    public function setAdvanceAttribute($value)
    {
        $this->attributes['advance'] = $value * 100;
    }

    public function setDeliveryDateAttribute($value)
    {
        $date = now()->createFromFormat('d.m.Y H:i', $value);

        if (! $date) {
            $date = now()->parse($value);
        }

        $this->attributes['delivery_date'] = $date;
    }

    public function getDeliveryDateAttribute($value)
    {
        return now()->parse($value)->format('d.m.Y H:i');
    }

    public function details()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function owner()
    {
        return $this->hasOne(User::class, 'id', 'created_by')
            ->withDefault(['id' => 0, 'name' => 'client']);
    }

    public function getTotalPrice()
    {
        if (! $this->relationLoaded('details')) {
            $this->load('details');
        }

        return $this->details->sum('price');
    }
}
