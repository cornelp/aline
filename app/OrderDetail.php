<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $fillable = [
        'order_id', 'product_id', 'quantity',
        'price', 'text', 'structure_id', 'weight',
        'for_no_people', 'message', 'decor'
    ];

    public function getWeightAttribute($value)
    {
        return $value / 100;
    }

    public function setWeightAttribute($value)
    {
        $this->attributes['weight'] = $value * 100;
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function structure()
    {
        return $this->belongsTo(Structure::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
